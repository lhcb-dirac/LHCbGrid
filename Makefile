OS = $(word 2,$(subst -, ,$(CMTCONFIG)))

MANIFEST = InstallArea/$(CMTCONFIG)/manifest.xml
XENV = InstallArea/$(CMTCONFIG)/LHCbGrid.xenv
DIST-TOOLS=dist-tools


PYTHON_VERSION=2.7.9.p1
PYTHON_VERSION_TWO=2.7
PYTHON_VERSION_TWODIGIT=27

# do not need any more runit in the client, only the server 
#all: $(XENV) $(XENV)c runit_tools
all: $(XENV) $(XENV)c


$(XENV) $(MANIFEST): Makefile
	mkdir -p InstallArea/$(CMTCONFIG)
	python $(DIST-TOOLS)/gen_xenv.py -c $(CMTCONFIG) -f $(XENV) -m $(MANIFEST) -p $(PYTHON_VERSION_TWO)

$(XENV)c: $(XENV)
	xenv --xml $(XENV) true


clean:
	$(RM) $(XENV) $(XENV)c $(MANIFEST) $(patsubst %,InstallArea/$(CMTCONFIG)/bin/%,$(RUNIT_TOOLS))

purge: clean
	$(RM) -r InstallArea

# fake targets to respect the interface of the Gaudi wrapper to CMake
configure:
install:
install/fast:
unsafe-install:
post-install:
