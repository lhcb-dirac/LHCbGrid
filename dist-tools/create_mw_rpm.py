#!/usr/bin/env python
'''

script to create the lcgBundle and the LHCbGrid tar file
to run it : lb-run LHCbDirac create_mw_rpm.py -t -l dev -p 2.7 -c x86_64-centos7-gcc62-opt -d

'''

__RCSID__ = 'joel.closier@cern.ch'

import os
import datetime
import sys
import commands
import argparse
import getopt
import shutil
import json
import logging

log = logging.getLogger()
log.setLevel( logging.INFO )
today = datetime.date.today()
date_of_the_day = today.strftime( '%Y' ) + '-' + today.strftime( '%m' ) + '-' + today.strftime( '%d' )
date_of_the_day = 'dev'

# file to list package and version used for bundle
FILENAME_VERSION = 'version-' + date_of_the_day + '.txt'
# Directory where the LCG GridMiddleware is installed
GRID_BASE = '/cvmfs/grid.cern.ch/'
AFS_BASE = '/afs/cern.ch/sw/lcg'
CVMFS_BASE = '/cvmfs/sft.cern.ch/lcg'
# Directory to store the tarball of lcgBundles
LCGBUNDLE_DIR = '/afs/cern.ch/lhcb/distribution/Dirac_project/lcgBundles'
# Directory to store the RPM
LCGRPM_DIR = '/eos/project/l/lhcbwebsites/www/lhcb-rpm/lcg'
# relation between LHCb convention and Dirac convention
# ConfigLink = {'x86_64-slc5-gcc43-opt':'Linux_x86_64_glibc-2.5', 'x86_64-slc6-gcc44-opt':'Linux_x86_64_glibc-2.12', 'x86_64-slc6-gcc48-opt':'Linux_x86_64_glibc-2.12', 'x86_64-slc6-gcc49-opt':'Linux_x86_64_glibc-2.12', 'x86_64-centos7-gcc48-opt' : 'Linux_x86_64_glibc-2.17', 'x86_64-slc6-gcc62-opt':'Linux_x86_64_glibc-2.12'}
ConfigLink = {'x86_64-slc6-gcc49-opt':'Linux_x86_64_glibc-2.12', 'x86_64-centos7-gcc62-opt' : 'Linux_x86_64_glibc-2.17'}
# define TMPDIR
if os.environ.has_key( 'TMPDIR' ):
  RPMTMPDIR = os.environ['TMPDIR']
else:
  RPMTMPDIR = '/tmp/jojo'


List_Pkg = []
specialConfig = 'projectConfig.json'

def loadConfig( dir = None, fname = 'projectConfig.json' ):
  '''
  Loads the project configuration from the JSON file
  '''
  if dir != None:
    fname = os.path.join( dir, fname )

  res = None
  if os.path.exists( fname ):
    log.info( 'Reading the config from file ' + fname )
    with open( fname ) as f:
      res = json.load( f )
  else:
    log.info( 'The file ' + fname + ' does not exist\n' )

  return res

def concatConfig( configA, configB ):
  '''
  Concatenate the project configuration from the JSON file
  '''
  glob_data = {}
  glob_data.update( configA )
  glob_data.update( configB )

  configfile = 'projectConfigConcat.json'
  with open( configfile, 'w' ) as f:
    json.dump( glob_data, f, indent = 2 )

  f.close()

  return configfile

def usage():
  print "usage : [--test] --LHCbGridVer= (version of LHCbGrid) --pythonver=(verion of python A.B) --cmtconfig=(COMTCONFIG version)"
  sys.exit( 1 )

def delete_tmpdir():
  cmdDel = 'rm -rf ' + tmpDir + '/*'
  log.info( '\n###  Deleting the temporary directory ' + tmpDir )
  status, result = commands.getstatusoutput( cmdDel )

def prepare_tmpdir( pkg_name, pkg_version, pkg_dir ):
  '''
  copy the package in the temporary directory
  '''
  if pkg_dir == 'exttools':
    pkg_fullpath = os.path.join( 'external', 'Grid', pkg_name, pkg_version, CMTCONFIG )
    BASE = AFS_BASE
  elif pkg_dir == 'heptools':
    pkg_fullpath = os.path.join( 'releases', LCG_REPLACE, pkg_name, pkg_version, CMTCONFIG )
    BASE = CVMFS_BASE
  elif pkg_dir == 'LCGCMT':
    pkg_fullpath = os.path.join( 'releases', pkg_dir, pkg_dir + '_' + pkg_version )
    BASE = CVMFS_BASE

  logging.info( 'Copying the Applications in the temporary directory ' )
  if not os.path.exists( os.path.join( BASE, pkg_fullpath ) ):
    log.error( 'This path does not exist ' + os.path.join( BASE, pkg_fullpath ) )
    return -1

  cmd = 'cd ' + BASE + ' ; tar chf - ' + pkg_fullpath + ' | (cd ' + tmpDir + ' ;  tar xf -)'
  log.debug( cmd )
  status, result = commands.getstatusoutput( cmd )
  log.debug( "JCCCC prepare:" + str( status ) + ',' + result )
  if status != 0:
    log.error( result )

  return status

def create_lcgBundle( pkg_name, pkg_version, pkg_dir ):
  '''
  create the tar ball for lcgBundle
  '''
  log.info( '\nCreate lcgBundle for ' + pkg_name + ' ' + pkg_version )

  commands.getstatusoutput( 'cd ' + tmpDir )
  statuscmd = prepare_tmpdir( pkg_name, pkg_version, pkg_dir )
  if statuscmd != 0:
    return statuscmd

  if pkg_dir == 'exttools':
    if not os.path.exists( os.path.join( tmpDir, ConfigLink[CMTCONFIG] ) ):
      os.mkdir( os.path.join( tmpDir, ConfigLink[CMTCONFIG] ) )

    cmd = 'cp -r ' + os.path.join( tmpDir, 'external', 'Grid', pkg_name, pkg_version, CMTCONFIG ) + '/* ' + os.path.join( tmpDir, ConfigLink[CMTCONFIG] )
    log.debug( "===> " + cmd )
    status, result = commands.getstatusoutput( cmd )
    log.debug( "JCCCC " + str( status ) + ',' + result )
    if status != 0:
      log.error( result )
      return status

  if pkg_dir == 'heptools':
    if not os.path.exists( os.path.join( tmpDir, ConfigLink[CMTCONFIG] ) ):
      os.mkdir( os.path.join( tmpDir, ConfigLink[CMTCONFIG] ) )

    cmd = 'cp -r ' + os.path.join( tmpDir, 'releases', LCG_REPLACE, pkg_name, pkg_version, CMTCONFIG ) + '/* ' + os.path.join( tmpDir, ConfigLink[CMTCONFIG] )
    log.debug( "===> " + cmd )
    status, result = commands.getstatusoutput( cmd )
    log.debug( "JCCCC " + str( status ) + ',' + result )
    if status != 0:
      log.error( result )
      return status
    if pkg_name == 'xrootd':
      log.debug( os.path.join( tmpDir, 'releases', LCG_REPLACE, pkg_name, pkg_version, CMTCONFIG ) )

  for tmppath in os.listdir( tmpDir ):
      if not tmppath in ( ConfigLink[CMTCONFIG], FILENAME_VERSION ):
          shutil.rmtree( os.path.join( tmpDir, tmppath ) )
  # as DIRAC do not understand lib64, I copy the content of lib64 to lib
  if not os.path.exists( os.path.join( tmpDir, ConfigLink[CMTCONFIG], 'lib' ) ):
    os.mkdir( os.path.join( tmpDir, ConfigLink[CMTCONFIG], 'lib' ) )

  if os.path.exists( os.path.join( tmpDir, ConfigLink[CMTCONFIG], 'lib64' ) ):
    cmd = 'cp -r ' + os.path.join( tmpDir, ConfigLink[CMTCONFIG], 'lib64/*' ) + ' ' + os.path.join( tmpDir, ConfigLink[CMTCONFIG], 'lib' )
    log.debug( "===> " + cmd )
    status, result = commands.getstatusoutput( cmd )
    log.debug( "JCCCC " + str( status ) + ',' + result )
    if status != 0:
      log.error( result )
      return status

    cmd = 'rm -r ' + os.path.join( tmpDir, ConfigLink[CMTCONFIG], 'lib64' )
    log.debug( "===> " + cmd )
    status, result = commands.getstatusoutput( cmd )
    log.debug( "JCCCC " + str( status ) + ',' + result )
    if status != 0:
      log.error( result )
      return status
  return 0

def create_rpmGrid( pkg_name, pkg_version, pkg_dir ):
  '''
  create all the RPM for the dependencies of LHCbGrid
  '''

  LCGRPMPath = os.path.join( '.', 'LCGRPM', 'package' )
  if not os.path.exists( LCGRPMPath ):
    log.error( LCGRPMPath + ' do not exist' )
    sys.exit( 0 )
  RPMSDIR = os.path.join( RPMTMPDIR, 'rpmbuild/RPMS/noarch' )
  if not os.path.exists( RPMSDIR ):
    status, result = commands.getstatusoutput( 'mkdir -p ' + RPMSDIR )
    if status != 0:
      log.error( result )

  if not os.path.exists( os.path.join( LCGRPM_DIR, pkg_name + '_' + pkg_version.replace( '-', '_' ) + '_' + CMTCONFIG.replace( '-', '_' ) + '-1.0.0-1.noarch.rpm' ) ) and pkg_name != 'LCG':
    log.info( '\nCreate RPM for ' + pkg_name + ' ' + pkg_version )

    if pkg_dir == 'exttools':
      pkg_source = os.path.join( 'external', 'Grid' )
      BASE = AFS_BASE
    else:
      pkg_source = os.path.join( 'releases', LCG_REPLACE )
      BASE = CVMFS_BASE

    cmdSpec = LCGRPMPath + '/createExternalRPMSpec.py  -d -b ' + RPMTMPDIR + ' -l ' + BASE + ' -t ' + pkg_source + ' -o ext.spec ' + pkg_name + ' ' + pkg_version + ' ' + CMTCONFIG
    log.debug( cmdSpec )
    status, result = commands.getstatusoutput( cmdSpec )
    log.debug( "JOJOJ ===" + str( status ) + ' ' + result )
    if status == 0:
      log.info( result )
      cmdSpecBuild = 'rpmbuild -bb ext.spec'
      status, result = commands.getstatusoutput( cmdSpecBuild )
      if status != 0:
        log.error( result )

  for rpmfile in os.listdir( RPMSDIR ):
    log.info( rpmfile + ',' + os.path.join( LCGRPM_DIR, rpmfile ) )
    if os.path.exists( os.path.join( LCGRPM_DIR, rpmfile ) ):
      log.info( 'rpmfile already exist ' + rpmfile )
    else:
      log.info( ' I will copy the file ' + rpmfile )
      cmdCopy = 'cp ' + os.path.join( RPMSDIR, rpmfile ) + ' ' + os.path.join( LCGRPM_DIR, rpmfile )
      status, result = commands.getstatusoutput( cmdCopy )
      if status != 0:
        log.error( result )
      else:
        cmdRm = 'rm ' + os.path.join( RPMSDIR, rpmfile )
        status, result = commands.getstatusoutput( cmdRm )
        log.info( result )

# set logging
logging.basicConfig( stream = sys.stderr )

# Parsing options
parser = argparse.ArgumentParser()
parser.add_argument( '-d', '--debug',
                  dest = "debug",
                  default = False,
                  action = "store_true",
                  help = "Show debug information" )
parser.add_argument( '-p', '--pythonver',
                  dest = "pythonVersion",
                  default = None,
                  action = "store",
                  help = "Set the python version" )
parser.add_argument( '-c', '--cmtconfig',
                  dest = "CMTCONFIG",
                  default = None,
                  action = "store",
                  help = "Set the cmtconfig version" )
parser.add_argument( '-l', '--LHCbDiracVer',
                  dest = "lbdiracVer",
                  default = None,
                  action = "store",
                  help = "Set the LHCbDirac version" )
parser.add_argument( '-t', '--test',
                  dest = "dirConfig",
                  default = None,
                  action = "store_true",
                  help = "Use a test directory" )

args = parser.parse_args()

if args.debug:
  log.setLevel( logging.DEBUG )

if args.dirConfig != None:
  if 'LCGBUNDLESTEMPDIR' in os.environ:
    dirConfig = os.environ['LCGBUNDLESTEMPDIR']
  else:
    log.error( 'variable LCGBUNDLESTEMPDIR is not set' )
    sys.exit ( 0 )
else:
  dirConfig = '/cvmfs/lhcb.cern.ch/lib/lhcb/'

if not ConfigLink.has_key( args.CMTCONFIG ):
  log.error( "CMTCONFIG %s not known " % args.CMTCONFIG )
  sys.exit( 1 )
else:
  CMTCONFIG = args.CMTCONFIG
  GCCCONFIG = CMTCONFIG.split( '-' )[2]

# pythonVersion = opts.pythonVersion
# if opts.pythonVersion.find( '.' ):
pythonVersion = args.pythonVersion
if args.pythonVersion.find( '.' ):
  pythonversion_short = pythonVersion.split( '.' )[0] + pythonVersion.split( '.' )[1]

lbdiracVer = args.lbdiracVer
log.debug( dirConfig )
log.debug( CMTCONFIG )
log.debug( GCCCONFIG )
log.debug( pythonVersion )
log.debug( lbdiracVer )

# define LHCBRELEASES area
lcgVer = None
#  Directory where the work will be done temporarely
tmpDir = os.path.join( RPMTMPDIR, 'Grid' )
if not os.path.exists( tmpDir ):
  os.makedirs( tmpDir )
else:
  delete_tmpdir()

log.info( '\n### Create the RPM for LHCbGrid ' + lbdiracVer )

# check The dependencies for LHCbDirac
specialConfigDir = os.path.join( dirConfig, 'LHCBDIRAC', 'LHCBDIRAC_' + lbdiracVer, 'dist-tools' )
if os.path.exists( os.path.join( specialConfigDir, specialConfig ) ):
  config = loadConfig( specialConfigDir, specialConfig )
  if os.path.exists( os.path.join( specialConfigDir, 'heptools_' + CMTCONFIG + '.json' ) ):
    configDirac = loadConfig( specialConfigDir, 'heptools_' + CMTCONFIG + '.json' )
  else:
    configDirac = loadConfig( specialConfigDir, 'heptools.json' )
else:
  log.info( "Path does not exist :" + os.path.join( specialConfigDir , specialConfig ) )
  sys.exit( 0 )

# find the version of LHCbGrid used by LHCbDirac
if config == None:
  log.info( "no config file\n" )
  sys.exit( 0 )
else:
  for lb_pkg in config['used_projects']['project']:
    if lb_pkg[0] == 'LHCbGrid':
      lcgVer = lb_pkg[1]

# check dependencies of LHCbGrid
if os.path.exists( os.path.join( dirConfig, 'LHCBGRID/LHCBGRID_' + lcgVer + '/dist-tools/projectConfig_' + GCCCONFIG + '.json' ) ):
  configGrid = loadConfig( os.path.join( dirConfig, 'LHCBGRID/LHCBGRID_' + lcgVer + '/dist-tools/', 'projectConfig_' + GCCCONFIG + '.json' ) )
else:
  if os.path.exists( os.path.join( dirConfig, 'LHCBGRID/LHCBGRID_' + lcgVer + '/dist-tools/' + specialConfig ) ):
    configGrid = loadConfig( os.path.join( dirConfig , 'LHCBGRID/LHCBGRID_' + lcgVer + '/dist-tools/' ) )
  else:
    log.info( "Path does not exist :" + os.path.join( dirConfig , 'LHCBGRID/LHCBGRID_' + lcgVer + '/dist-tools/' + specialConfig ) )
    sys.exit( 0 )

configBundle = loadConfig( './', concatConfig( configDirac, configGrid ) )

config = configBundle

if not os.path.exists( os.path.join( tmpDir, FILENAME_VERSION ) ):
  fd = open( os.path.join( tmpDir, FILENAME_VERSION ), 'w' )
else:
  fd = open( os.path.join( tmpDir, FILENAME_VERSION ), 'a' )

for opt in ( 'exttools', 'heptools' ):
  if opt in config:
    if 'version' in config[opt]:
      LCG_REPLACE = 'LCG_' + config[opt].get( 'version' )
    if 'packages' in config[opt]:
      packagesConfig = config[opt].get( 'packages' )
      for pack_info in packagesConfig:
        ( p, v, t ) = pack_info[0:3]
        statuscr = create_rpmGrid( p, v, opt )
        log.debug( statuscr )
        if p not in ( 'pyparsing' , 'm2crypto', 'pathlib2', 'pickleshare', 'backports', 'pexpect', 'prompt_toolkit', 'wcwidth', 'simplegeneric', "pygsi", 'mysql', 'Python', 'gcc', 'ipython_genutils', 'qt', 'pytools', 'pygraphics' ):
          statuslcg = create_lcgBundle( p, v, opt )
          if statuslcg == 0:
            fd.write( p + ', ' + v + '\n' )

fd.close()
log.info( 'Rebuild the database' )
cmdRepo = 'createrepo --workers=20 --update ' + LCGRPM_DIR
status, result = commands.getstatusoutput( cmdRepo )
if status != 0:
  log.debug( result )

cmdTar = 'cd ' + tmpDir + '; tar zcf ' + os.path.join( LCGBUNDLE_DIR, 'DIRAC-lcg-' + date_of_the_day + '-' + ConfigLink[CMTCONFIG] + '-python' + pythonversion_short + '.tar.gz' ) + ' .'
log.info( cmdTar )
status, result = commands.getstatusoutput( cmdTar )
if status != 0:
  print result

sys.exit( 0 )
