This project will define the grid middleware dependencies.

Prepare the environemnt to generate and test LHCbGrid and lcgbundles

- export LCGBUNDLESTEMPDIR="your-working-directory"
- export DIRAC_VERSION=vXrYpZ (from https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/blob/master/dist-tools/projectConfig.json)
- export LHCBDIRAC_VERSION=vArBpC (from https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/blob/master/dist-tools/projectConfig.json)
- export LHCBGRID_VERSION=vDrEpF (from https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC/blob/master/dist-tools/projectConfig.json)
- cd $LCGBUNDLESTEMPDIR

Install Dirac, LHCbGrid and LHCbDirac as "dev"

- git clone ssh://git@gitlab.cern.ch:7999/lhcb-dirac/LHCbDIRAC.git LHCBDIRAC/LHCBDIRAC_dev
- git clone ssh://git@gitlab.cern.ch:7999/lhcb-dirac/LHCbGrid.git LHCBGRID/LHCBGRID_dev
- mkdir -p DIRAC/DIRAC_dev
- git clone https://github.com/DIRACGrid/DIRAC.git DIRAC/DIRAC_dev/DIRAC
- cp -r /cvmfs/lhcb.cern.ch/lib/lhcb/DIRAC/DIRAC_$DIRAC_VERSION/etc DIRAC/DIRAC_dev/
- cp -r /cvmfs/lhcb.cern.ch/lib/lhcb/DIRAC/DIRAC_$DIRAC_VERSION/manifest.xml DIRAC/DIRAC_dev/
- cp -r /cvmfs/lhcb.cern.ch/lib/lhcb/DIRAC/DIRAC_$DIRAC_VERSION/Dirac.xenv DIRAC/DIRAC_dev/
- cp -r /cvmfs/lhcb.cern.ch/lib/lhcb/DIRAC/DIRAC_$DIRAC_VERSION/Makefile DIRAC/DIRAC_dev/

Replace version by dev
- perl -pi.bak -e s/${DIRAC_VERSION}/dev/ DIRAC/DIRAC_dev/manifest.xml
- perl -pi.bak -e s/${DIRAC_VERSION}/dev/ LHCBDIRAC/LHCBDIRAC_dev/dist-tools/projectConfig.json
- perl -pi.bak -e s/${LHCBDIRAC_VERSION}/dev/ LHCBDIRAC/LHCBDIRAC_dev/dist-tools/projectConfig.json (for dirac)
- perl -pi.bak -e s/${LHCBGRID_VERSION}/dev/ LHCBDIRAC/LHCBDIRAC_dev/dist-tools/projectConfig.json (for lhcbgrid)

get LCGRPM
- tar zxvf /afs/cern.ch/lhcb/distribution/Dirac_project/lcgBundles/LCGRPM.tar.gz

-export CMTPROJECTPATH=$LCGBUNDLESTEMPDIR
- cd  $LCGBUNDLESTEMPDIR/LHCBGRID/LHCBGRID_dev
- make all
- cd - $LCGBUNDLESTEMPDIR/LHCBDIRAC/LHCBDIRAC_dev
- make all

Once you have your environment, you can "checkout the correct version of LHCbDirac and LHCbGrid" to build the lcgBundles and check that all the RPM needed are present.

- cd $LCGBUNDLESTEMPDIR
- lb-run LHCbDirac ${LCGBUNDLESTEMPDIR}/LHCBGRID/LHCBGRID_dev/dist-tools/create_mw_rpm.py -t -l dev -p 2.7 -c x86_64-slc6-gcc49-opt
( to be done for all python version 2.6 and 2.7 and all platform x86_64-slc6-gcc48-opt, x86_64-slc6-gcc49-opt, x86_64-slc6-gcc62-opt and x86_64-centos7-gcc62)

it will create a file called /afs/cern.ch/lhcb/distribution/Dirac_project/lcgBundles/DIRAC-lcg-dev-Linux_x86_64_glibc-2.17-python27.tar.gz that you can rename as you want.

